# -*- coding: utf-8 -*-
from ucimlrepo import fetch_ucirepo
import logging
from pathlib import Path


def download_from_UCI(raw_data_path, dataset_id=296) -> None:
    """
   Downloads a dataset from the University of California Irvine (UCI) 
   repository and saves it in CSV format.

   Parameters
   ----------
   raw_data_path : str
       The path where the dataset will be saved in CSV format.
   dataset_id : int, optional
       The ID of the dataset in the UCI repository (default is 296).

   Returns
   -------
   None
       This function doesn't return any value.
   """
    data_set_hospital_readmision = fetch_ucirepo(id=296)

    logger = logging.getLogger(__name__)
    logger.info('downloading data')

    data_set_hospital_readmision.data.original.to_csv(
        raw_data_path + '/' + data_set_hospital_readmision.
        metadata.name.replace(' ', '_') + '.csv', index=False)


if __name__ == '__main__':

    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    project_dir = Path(__file__).resolve().parents[2]

    download_from_UCI(raw_data_path=str(project_dir) + '/data/raw')
